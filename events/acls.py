from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    try:
        # Create a dictionary for the headers to use in the request
        headers = {'Authorization': PEXELS_API_KEY}
        # Create the URL for the request with the city and state
        url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
        # Make the request
        response = requests.get(url, headers=headers)
        # Parse the JSON response
        result = json.loads(response.content)
        if 'photos' in result:
            picture_url = result['photos'][0]["url"]
        else:
            picture_url = None
        # Return a dictionary that contains a `picture_url` key and
        #   one of the URLs for one of the pictures in the response
        return {'picture_url': picture_url}

    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    # Create the URL for the geocoding API with the city and state
    # Make the request
    response = requests.get(url)
    print("Geocoding API Response:", response.content)
    # Parse the JSON response
    result_1 = json.loads(response.content)
    print("Geocoding API Result:", result_1)
    # Get the latitude and longitude from the response
    # lat = None
    # lon = None
    # if 'lat' in result_1 and 'lon' in result_1:
    lat = result_1[0]["lat"]
    lon = result_1[0]["lon"]

    # Create the URL for the current weather API with the latitude and longitude
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url)
    print("Current Weather API Response:", response.content)
    # Parse the JSON response
    result_2 = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    temperature = None
    description = None
    # if 'temp' in result_2 and 'description' in result_2:
    temperature = result_2["main"]["temp"]
    description = result_2["weather"][0]["description"]
    weather = {"temperature": temperature, "description": description}
    # Return the dictionary
    return {'weather': weather}

